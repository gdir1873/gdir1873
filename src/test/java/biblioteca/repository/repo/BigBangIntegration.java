package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BigBangIntegration {
    private static CartiRepoMock cartiRepoMock;

    @BeforeClass
    public static void  initialize(){
        cartiRepoMock = new CartiRepoMock();
    }

    @Test
    public void unitTestA() throws Exception {
        String titlu="titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("Ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "2005";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);

        int initialSize = cartiRepoMock.getCarti().size();
        cartiRepoMock.adaugaCarte(c);
        int newSize = cartiRepoMock.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize+1;
        Assert.assertEquals(expected_size,actual_result);
    }

    @Test
    public void unitTestB() {
        String ref = "Mihai";
        int dim_expected=1;
        int dim_found = cartiRepoMock.cautaCarte(ref).size();
        Assert.assertEquals(dim_expected,dim_found);
    }

    @Test
    public void unitTestC(){
        String an = "1948";
        int dim_expected=3;
        int dim_found = cartiRepoMock.getCartiOrdonateDinAnul(an).size();
        Assert.assertEquals(dim_expected,dim_found);
        Assert.assertEquals("Dale carnavalului", cartiRepoMock.getCartiOrdonateDinAnul(an).get(0).getTitlu());
    }

    @Test
    public void integrare() throws Exception {
        //P -> A -> C -> B
        int c = cartiRepoMock.getCarti().size();
        int dim_expected=c+1;
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Titlu;Autor;1948;Star;roman"));
        int dim_found = cartiRepoMock.getCarti().size();
        Assert.assertEquals(dim_expected,dim_found);

        List<Carte> list = cartiRepoMock.getCartiOrdonateDinAnul("1948");
        Assert.assertEquals(4, list.size());
        Assert.assertEquals("Caragiale Ion", list.get(0).getReferenti().get(0));

        List<Carte> result = cartiRepoMock.cautaCarte("Eminescu");
        Assert.assertEquals(1, result.size());
    }
}
