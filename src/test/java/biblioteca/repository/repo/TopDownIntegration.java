package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class TopDownIntegration {
    private static CartiRepoMock cartiRepoMock;

    @BeforeClass
    public static void  initialize(){
        cartiRepoMock = new CartiRepoMock();
    }

    @Test
    public void unitTestA() throws Exception {
        int c = cartiRepoMock.getCarti().size();
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Titlu;Autor;1948;Star;roman"));
        Assert.assertEquals(c + 1, cartiRepoMock.getCarti().size());
    }

    @Test
    public void TestB() throws Exception { //A si B
        //P -> A -> B
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Titluu;Autoor;1948;Star;poezii"));
        List<Carte> list = cartiRepoMock.cautaCarte("Autoor");
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void TestC() throws Exception {// A,B si C
        // P -> C -> B -> A
        List<Carte> list = cartiRepoMock.getCartiOrdonateDinAnul("1948");
        Assert.assertEquals(3, list.size());
        Assert.assertEquals("Enigma Otiliei", list.get(1).getTitlu());
        Assert.assertEquals(2, list.get(1).getCuvinteCheie().size());


        List<Carte> list2 = cartiRepoMock.cautaCarte("Eminescu");
        Assert.assertEquals(1, list2.size());


        int c = cartiRepoMock.getCarti().size();
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Titlu;Autor;1948;Star;roman"));
        Assert.assertEquals(c + 1, cartiRepoMock.getCarti().size());
    }
}
