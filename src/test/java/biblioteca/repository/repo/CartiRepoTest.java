package biblioteca.repository.repo;

import biblioteca.begin.Start;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by diana on 31.03.2018.
 */
public class CartiRepoTest {
    private BibliotecaCtrl ctrl;
    private BibliotecaCtrl ctrlMock;

    @Before
    public void setUp() throws Exception{
        CartiRepo repo = new CartiRepo();
        ctrl=new BibliotecaCtrl(repo);
        CartiRepoMock repoM = new CartiRepoMock();
        ctrlMock = new BibliotecaCtrl(repoM);
    }
    @Test
    public void Req2_Tests() throws Exception {
        SearchTest_1();
        SearchTest_2();
        SearchTest_3();
        SearchTest_4();
    }

    @Test
    public void Req3_Tests() throws Exception{
        Req3_TestValid();
        Req3_TestInvalid();
    }

    public void Req3_TestInvalid() throws Exception {
        String an = "abcd";
        try{
        ctrl.getCartiOrdonateDinAnul(an);}
        catch (Exception ex){
            assertEquals("Nu e numar!", ex.getMessage());
        }
    }

    public void Req3_TestValid() throws Exception {
        String an = "1973";
        int dim_expected=2;
        int dim_found = ctrl.getCartiOrdonateDinAnul(an).size();
        assertEquals(dim_expected,dim_found);
        assertEquals("Poezii", ctrl.getCartiOrdonateDinAnul(an).get(0).getTitlu());
    }

    @Test
    public void TC1_ECP() throws Exception {
        int initialSize = ctrl.getCarti().size();
        String titlu="titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("Ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "2005";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);
            System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC1: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC2_ECP() throws Exception{
        //titlu invalid
        int initialSize = ctrl.getCarti().size();
        String titlu="titlu1";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "2005";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
        ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC2: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC3_ECP() throws Exception{
        //editura invalida
        int initialSize = ctrl.getCarti().size();
        String titlu="titlu";
        String editura="editura1";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "2005";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC3: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC4_ECP() throws Exception{
        //valid
        int initialSize = ctrl.getCarti().size();
        String titlu="titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "21894649";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC4: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC5_ECP() throws Exception{
        //referent invalid
        int initialSize = ctrl.getCarti().size();
        String titlu="titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref1");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "100";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
        ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC5: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC6_BVA() throws Exception{
        //titlu invalid; length = 0;
        int initialSize = ctrl.getCarti().size();
        String titlu="";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "35";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC6: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC7_BVA() throws Exception{
        //
        int initialSize = ctrl.getCarti().size();
        String titlu="aaa";
        String editura="aa";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "86";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC7: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC8_BVA() throws Exception{
        //
        int initialSize = ctrl.getCarti().size();
        String titlu="aaa";
        String editura="aa";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "-1";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC8: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC9_BVA() throws Exception{
        //valid daca lista autori vida
        int initialSize = ctrl.getCarti().size();
        String titlu="aaa";
        String editura="aa";
        ArrayList<String> listaRef = new ArrayList<String>();
        //listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "1423";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC9: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC10_BVA() throws Exception{
        //titlu invalid -> 3 stringuri
        int initialSize = ctrl.getCarti().size();
        String titlu="aa aa aa";
        String editura="aa";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("ref");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        String an = "1423";
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC10: "+ex.getMessage());}
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }

    @Test
    public void SearchTest_1() throws Exception{
        String ref = "Mihai";
        int dim_expected=2;
        int dim_found = ctrl.cautaCarte(ref).size();
        assertEquals(dim_expected,dim_found);

    }


    @Test
    public void SearchTest_2() throws Exception{
        String ref = "abcd";
        int dim_expected=0;
        int dim_found = ctrl.cautaCarte(ref).size();
        assertEquals(dim_expected,dim_found);
    }

    @Test
    public void SearchTest_3() throws Exception{
        String ref = "aaaaaaa";
        int dim_expected=0;
        int dim_found = ctrlMock.cautaCarte(ref).size();
        assertEquals(dim_expected,dim_found);
    }

    @Test
    public void SearchTest_4() throws Exception{
        String ref = "zs";
        int dim_expected=0;
        int dim_found = ctrl.cautaCarte(ref).size();
        assertEquals(dim_expected,dim_found);
    }

}
